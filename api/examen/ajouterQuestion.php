<?php
require_once "../../modules/Examen.php";
require_once "../../Connection.php";
if(isset($_POST['id_questions'])):
    $pdo = new Connection();
    $data = json_decode($_POST['id_questions']);
    $examen = new Examen();
    $examen->setId((int)$_POST['id_examen']);
    $examen->addQuestionInExam($pdo->connect(),$data);
endif;