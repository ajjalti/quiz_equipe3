<?php
session_start();

$title="details evaluation";
require_once("../config/header.php");

require_once '../../Connection.php';
require_once "../../modules/Formateur.php";
require_once "../../modules/Examen.php";
require_once "../../modules/Question.php";
require_once "../../modules/Reponse.php";
require_once "../../modules/Evaluation.php";
require_once "../../modules/DetailEvaluation.php";

// connexion : 
$db = new Connection();
$conn = $db->connect();


$data = DetailEvaluation::findByEvaluation($conn,$_GET['id']);
$questions = [];
$reponses = [];
foreach ($data as $d) {
    $question = Question::findById($conn,$d->getIdQuestion());
    array_push($questions,$question);
    $reponse = Reponse::findById($conn,$d->getIdReponse());
    array_push($reponses,$reponse);
}
$evaluation = Evaluation::findById($conn,$_GET['id'])[0];
?>

<div>
<div class="row">
        <div class="mb-3 col-6">
            <label class="form-label">numEvaluation</label>
            <input type="text" class="form-control" value="<?= $evaluation->getId() ?>" readonly>
        </div>
        <div class="mb-3 col-6">
            <label class="form-label">Examen</label>
            <input type="text" class="form-control" value="<?= $evaluation->getExamen($conn)->getLib() ?>" readonly>
        </div>
        </div>     
        <?php 
        $count = 1;
        foreach($data as $d):
        ?>
            <div class="row"> 
        <?php foreach ($questions as $question): ?>  
        <div class="mb-3 col-6">
            <label class="form-label">question <?= $count ?></label>
            <input type="text" class="form-control" value="<?= $question->getLib() ?>" readonly>
        </div>
        <?php 
        endforeach;
        foreach($reponses as $reponse):
        ?>
        <div class="mb-3 col-6">
            <label class="form-label">reponse choisi</label>
            <input type="text" class="form-control" value="<?= $reponse->getLib() ?>" readonly>
        </div>
        <?php endforeach; ?>
        </div>
        <?php 
        $count++;
        endforeach;
        ?>
<a href="./evaluations" class="btn btn-secondary">retour vers liste d'evaluations</a>
</div>