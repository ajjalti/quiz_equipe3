<?php
session_start();

$title="Evaluation";
require_once("../config/header.php");

require_once '../../Connection.php';
require_once "../../modules/Formateur.php";
require_once "../../modules/Stagiaire.php";
require_once "../../modules/Examen.php";
require_once "../../modules/Evaluation.php";
require_once "../../modules/DetailEvaluation.php";

// connexion : 
$db = new Connection();
$conn = $db->connect();

// current logged in formateur id (should be stocked in session)
$evaluations = Evaluation::findAllByFormateur($conn,'2');
//var_dump($evaluations);
// $data = DetailEvaluation::findByEvaluation($conn,1);
// var_dump($data);
?>

<a href="../menu" class="btn btn-info mt-3">menu</a> 
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Stagiaire</th>
      <th scope="col">Examen</th>
      <th scope="col">score</th>
      <th scope="col">date passation</th>
      <th scope="col">options</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($evaluations as $eval) : ?>
    <tr>
      <td scope="row"><?= $eval->getId() ?></td>
      <td><?= $eval->getStagiaire($conn,$eval->getIdStagiaire())->getNom() ?></td>
      <td><?= $eval->getExamen($conn)->getLib() ?></td>
      <td><?= $eval->getScore() ?></td>
      <td><?= $eval->getDate() ?></td>
      <td>
        <a class="btn btn-success" href="detail?id=<?= $eval->getId() ?>">afficher détails</a>
        <a class="btn btn-danger" href="supprimer?id=<?= $eval->getId() ?>">supprimer</a>
      </td>
    </tr>
    <?php endforeach ?>
    
  </tbody>
</table>

<?php
require_once("../config/footer.php");
?>
