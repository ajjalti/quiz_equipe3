<?php
session_start();

require_once '../modules/Formateur.php';
require_once '../modules/Stagiaire.php';

$user = unserialize($_SESSION['user']);

if(!isset($_SESSION["user"])){
    header("Location: ../login.php");
}
//partie test
$type = null;
if ($user instanceof  Formateur) {
    $type = 'formateur';
} elseif ($user instanceof  Stagiaire) {
    $type = 'stagaire';
}
$_SESSION['user'] = serialize($user);

?>
<!--  partie formateur -->
<?php if ($type = 'formateur') : ?>
    <nav>
    <ul>
            <li>
                <a href="./examen/examens">gerer Examen</a>
            </li>
        </ul>        
        <ul>
            <li>
                <a href="./evaluation/evaluations">gerer Evaluation</a>
            </li>
        </ul>
    </nav>
<!-- partie sytagiaire -->
<?php elseif ($type = 'stagiaire') : ?>
    <nav>
        <ul>
            <li><a href="./passerExamen.php">passer Examen</a></li>
        </ul>
    </nav>
<?php endif; ?>

<a href="../router/logoutRouter.php"> Deconnecter </a>
