<?php
session_start();
$title = "Modifier";
require_once("../config/header.php");
require_once "../../modules/Examen.php";
require_once '../../Connection.php';
if(!isset($_GET["id"])){
    header("location:./");
}

if(isset($_POST["modifierExamen"])){
    // instancier un objet Connection:
    $db = new Connection();
    $conn = $db->connect();

    //Modifier l'examen
    $id = $_POST["id"];
    $libExamane = $_POST["libExamen"];
    $datePassation = $_POST["datePassation"];
    $examen = new Examen();
    $examen->modifierExamen($conn, $id,$libExamane,$datePassation);

    //Enregistrer les variables dans (URL)
    $idFiliere = $_SESSION['filiere'];
    $idModule = $_SESSION['module'];
    $idCompetence = $_SESSION['competence'];
    header("location:examens");
    
}else{
    $db = new Connection();
    $conn = $db->connect();
    $id = $_GET["id"];
    $examens = Examen::returnExamen($conn ,$id);
}

?>
<?php foreach ($examens as $examen) : ?> 
<div class="w-50 m-auto mt-5">
<form action="" method="post">
    <div class="mb-3">
        <div>
        <input hidden type="text" name="id" value="5">
            <label for="">libExamen</label>
            <input type="text" value="<?= $examen->getLibExamen() ?>" class="form-control" placeholder="libExamen" name="libExamen">
            <input hidden type="text" value="<?php echo $_GET["id"]; ?>" class="form-control" placeholder="libExamen" name="id">
        </div>
    </div>
    <div class="mb-3">
        <div>
            <label for="">Date de passation</label>
            <input type="date" value="<?= $examen->getDatePassation() ?>" class="form-control" placeholder="libExamen" name="datePassation">
        </div>
    </div>
    <button type="submit" name="modifierExamen" class="btn btn-info">Modefier l'examen</button>
</form>
</div>
<?php endforeach;?>



<?php require_once("../config/footer.php");?>