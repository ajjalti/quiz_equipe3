<?php
session_start();
$title = "Ajouter";
require_once("../config/header.php");
require_once "../../modules/Examen.php";
require_once "../../modules/Competence.php";
require_once "../../modules/Question.php";
require_once "../../modules/Formateur.php";
require_once '../../Connection.php';

// if(!isset($_SESSION["competence"])){
//     header("location:./");
// }
// instancier un objet Connection:
$db = new Connection();
$connect = $db->connect();


if(isset($_SESSION['filiere'],$_SESSION['module'],$_SESSION['competence'])):
    $filiere = Examen::getFiliere($connect,$_SESSION['filiere']);
    $module = Examen::getModule($connect,$_SESSION['module']);
    $competence = Examen::getCompetence($connect,$_SESSION['competence']);
    $count= Examen::getLastId($connect);
    $name = $filiere.'/'.$module.'/'.$competence.'/'.((int)$count+1);
endif;
if(isset($_POST["ajouter"])){
    //Ajouter un examen 
    $idCompetence = $_SESSION['competence'];
    $libExamen = $_POST["libExamen"];
    $dateCreation = date("Y-m-d");
    $datePassation = $_POST["datePassation"];
    $examens = new Examen();
    $user = unserialize($_SESSION['user']);
    $idExamen = $examens->ajouterExamen($connect,$idCompetence,$user->getId(),$libExamen,$dateCreation,$datePassation);

    echo $idExamen;
    //Ajouter des questions à l'examen

    if(isset($_POST["idQuestions"])){
        $idQuestions=$_POST["idQuestions"];
        $examen = new Examen();
        $examen->setId((int)$idExamen);
        $examen->addQuestionInExam($connect,$idQuestions);
    }
    header("location:examens.php");
    //! n'est pas util 
    // $idFiliere = $_SESSION['filiere'];
    // $idModule = $_SESSION['module'];
    // $idCompetence = $_SESSION['competence'];
    // header("location:index.php?filiere=$idFiliere&module=$idModule&competence=$idCompetence");
    
}

//Questions de Competence
$pdo = new Connection();
$competence = new Competence();
$competence->setId((int)$_SESSION['competence']);
$allQuestions=$competence->getQuestionForCompetence($pdo->connect());
?>

<div class="w-50 m-auto mt-5">
<form action="" method="post">

    <div class="mb-3">
        <div>
            <label for="">libExamen</label>
            <input type="text" class="form-control" placeholder="libExamen" value="<?= $name ?>" name="libExamen" id="libExamen" readonly>
        </div>
    </div>
    <div class="mb-3">
        <div>
            <label for="">Date de Passation</label>
            <input type="date" value="" class="form-control" name="datePassation">
        </div>
    </div>
<h6>ajouter question au examen</h6>
            <table border="1" class="table table-tesponsive table-stripped table-hover">
            <thead>
            <tr>
                <th>
                <input type="checkbox" name="addAll" id="addAll" onclick="selectAllA('idQuestions[]')">
                    select all
                </th>
                <th>#</th>
                <th>question</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($allQuestions as $question) : ?>
                    <!-- <form method="post"> -->
                        <tr data-id=<?= $question->getId() ?>>
                            <td><input type="checkbox" name="idQuestions[]" value=" <?=  $question->getId() ?>" ></td>
                            <td><?= $question->getId()?></td>
                            <td>
                            <?= $question->getLibQuestion() ?>
                            </td>
                        </tr>
                <?php endforeach;
                ?>
            </tbody>
                </table>
            <button type="submit" name="ajouter" class="btn btn-success mb-5">Ajouter Examen</button>
</form>

</div>
<script>

    const listA = document.getElementsByName("idQuestions[]")
    // add event listener:
    listA.forEach(element => {
        element.addEventListener("click", function(e){
        if(e.target.checked){
            e.target.parentElement.parentElement.classList.add("bg-secondary")
        }else{
            e.target.parentElement.parentElement.classList.remove("bg-secondary")
        }
     })
    })

        const selectAllA = (name)=>{
        const check = document.querySelector("#addAll")
        if(check.checked){
            const list = document.getElementsByName(name)
            for(let i=0; i<list.length; i++){
                list[i].checked=true
                list[i].parentElement.parentElement.classList.add("bg-secondary")
            }
        }
        if(!check.checked){
            const list = document.getElementsByName(name)
            for(let i=0; i<list.length; i++){
                list[i].checked=false
                list[i].parentElement.parentElement.classList.remove("bg-secondary")
            }
        }
    }

</script>




<?php require_once("../config/footer.php");?>