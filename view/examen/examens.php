<?php
session_start();

$title="Examen";
require_once("../config/header.php");
require_once "../../modules/Formateur.php";
require_once "../../modules/Stagiaire.php";
require_once "../../modules/Filiere.php";
require_once "../../modules/ModuleAssurer.php";
require_once "../../modules/Module.php";
require_once "../../modules/Competence.php";
require_once "../../modules/Examen.php";
require_once '../../Connection.php';

// connexion : 
$db = new Connection();
$conn = $db->connect();

if (!isset($_SESSION["user"])) {
    header("Location: ../login.php");
}
$user = unserialize($_SESSION['user']);
$total_page=0;
if(isset($_SESSION['filiere'],$_SESSION['module'],$_SESSION['competence'])):
    $idFiliere=$_SESSION['filiere'];
    $idModule=$_SESSION['module'];
    $idCompetence=$_SESSION['competence'];
    // remmetre les ancien informations:
    $modules = ModuleAssurer::retournerModules($conn, (int)$idFiliere, $user->getId());
    $competences = Module::retournerCompetences($conn, (int)$idModule);
    // $examens = Competence::returnerExamens($conn, (int)$idCompetence);

    if (isset($_GET['page'])) {
        $page = $_GET['page'];
      } else {
        $page = 1;
      }
    $data = Examen::pagination($conn,$page,(int)$idCompetence);
    $examens=$data['data'];
    $total_page=$data['totalPages'];
endif;

// echo $total_page;
// $filieres = [];
// $modules = [];
// $competences = [];
// $examens = [];



if ($user instanceof Formateur) {
    $filieres = $user->retournerFilieres($conn);
}

// $idFiliere = null;
// $idModule = null;
// $idCompetence = null;

//! ancien code de modification:!!
//!debut
// if (isset($_POST["update"])) {
//     $id = (int)$_POST['id'];
//     $label = $_POST['label'];
//     $datePassation = $_POST['datePassation'];
//     $examen = new Examen();
//     $examen->setId($id);
//     $examen->update($conn, $label, $datePassation);
//     $_POST["filiere"] = $_SESSION['filiere'];
//     $_POST["module"] = $_SESSION['module'];
//     $_POST["competence"] = $_SESSION['competence'];
// }
//!fin
if (isset($_POST["filiere"])) {
    $idFiliere = $_POST["filiere"];
    $_SESSION['filiere'] = $idFiliere;
    $modules = ModuleAssurer::retournerModules($conn, (int)$idFiliere, $user->getId());
}

if (isset($_POST["module"])) {
    $idModule = $_POST["module"];
    $_SESSION['module'] = $idModule;
    $competences = Module::retournerCompetences($conn, (int)$idModule);
}

if (isset($_POST["competence"]) && $_POST['competence']!="") {
    $idCompetence = $_POST["competence"];
    $_SESSION['competence'] = $idCompetence;
    $examens = Competence::returnerExamens($conn, (int)$idCompetence);
    header('location:./examens');
    // $competence = new Competence();
    // $competence->setId($idCompetence);
    // $examens = $competence->returner_Examens($conn);
}

function optionSelected($choix, $currentId): string
{
    return (isset($_SESSION[$choix]) && (int)$_SESSION[$choix] === $currentId) ? "selected" : "";
}

?>
    <div class="w-75 m-auto">
        <a class="btn btn-info mt-3" href="../menu"> menu </a>
        <form method="post">
            <div class="mt-1">
                <div>
                    <label for="">filiere</label>
                    <select required class="form-select" name="filiere" onchange="submit()" id="selectFiliere">
                        <!-- <option value="" hidden></option> -->
                        <option value="" selected disabled>--Choix du filiere--</option>
                        <?php foreach ($filieres as $filiere) : ?>
                            <option <?= optionSelected('filiere', $filiere->getId()) ?>
                                    value="<?= $filiere->getId() ?>"><?= $filiere->getLibFiliere() ?></option>
                        <?php endforeach;
                        // unset($filieres, $filiere);
                        ?>
                    </select>
                </div>
                <input type="hidden" name="s_filiere" value="filiere">
            </div>

            <div class="mt-1">
                <label for="">module</label>
                <select class="form-select" name="module" id="" onchange="submit()">
                    <option value="" selected disabled>--Choix du module--</option>
                    <?php if (isset($idFiliere)) : ?>
                        <?php foreach ($modules as $module) : ?>
                            <option <?= optionSelected('module', $module->getId()) ?>
                                    value="<?= $module->getId() ?>"><?= $module->getLibModule() ?></option>
                        <?php endforeach;
                        // unset($modules, $module);
                        ?>
                    <?php endif; ?>
                </select>
            </div>

            <div class="mt-1">
                <label for="">compétence</label>
                <select class="form-select" name="competence" id="" onchange="submit()">
                    <option value="" selected hidden>Choix du compétence</option>
                    <?php if (isset($idModule)) : ?>
                        <?php foreach ($competences as $competence) : ?>
                            <option <?= optionSelected('competence', $competence->getId()) ?>
                                    value="<?= $competence->getId() ?>"><?= $competence->getLibCompetence() ?></option>
                        <?php endforeach;
                        // unset($competences, $competence);
                        ?>
                    <?php endif; ?>
                </select>
            </div>
        </form>
    </div>

    <div class="w-75 m-auto mt-4">
        <table border="1" class="table table-stripped table-hover">
    <div class="container d-flex justify-content-between">
            <a href="ajouter.php" class="btn btn-success mb-3">ajouter</a>
            <a href="" class="btn btn-warning mb-3">Exporter |Excel</a>
    </div>
            <thead>
            <tr>
                <th>#</th>
                <th>Examen</th>
                <th>date creation</th>
                <th>date passation</th>
                <th>action</th>
            </tr>
            </thead>
            <tbody>
            <?php if (isset($idCompetence)) : ?>
                <?php foreach ($examens as $examen) : ?>
                    <!-- <form method="post"> -->
                        <tr>
                            <input hidden type="text" value="<?= $examen->getId() ?>" name="id">
                            <td><?= $examen->getId() ?></td>
                            <td>
                            <?= $examen->getLibExamen() ?>
                                <!-- <input type="text" name="label" value="<?= $examen->getLibExamen() ?>"> -->
                            </td>
                            <td><?= $examen->getDateCreation() ?></td>
                            <td>
                                <?= $examen->getDatePassation() ?>
                                <!-- <input name="datePassation" type="date" value="<?= $examen->getDatePassation() ?>"> -->
                            </td>

                            <td>
                                <!-- <a href="./editer.php?id=<?= $examen->getId() ?>"  class="btn btn-secondary"  >éditer</a> -->
                                <a href="../../router/examenRouter?id=<?= $examen->getId() ?>"  class="btn btn-secondary"  >éditer</a>
                                <a href="./modifier?id=<?= $examen->getId() ?>" class="btn btn-info"  >modifier</a>
                                <a id="supprimer" href="./supprimer?id=<?= $examen->getId() ?>" class="btn btn-danger" onclick="Message()"  >supprimer</a>
                            </td>
                        </tr>
                        <script>
                            function Message(){           
                            var result = confirm("Voulez-vous vraiment supprimer cet Examen ");
                            if (result != true) {
                                document.getElementById("supprimer").href = "examens.php";
                            } 
                            }
                        </script>
                    <!-- </form> -->
                <?php endforeach;
                unset($examens, $examen);
                ?>
            <?php endif; ?>
            </tbody>
            </table>
            <!-- pagination -->
            <nav class="Page navigation example">
            <ul class="pagination">
              <li class="page-item" class="<?php if ($page <= 1) {
                                              echo 'disabled';
                                            } ?>">
                <a class="page-link" href="<?php if ($page <= 1) {
                                              echo '#';
                                            } else {
                                              echo "?page=" . ($page - 1);
                                            } ?>">précédente</a>
              </li>
              <?php for($i=1;$i<$total_page+1;$i++):?>
              <li class="page-item"><a href="?page=<?=$i?>" class="page-link"><?= $i ?></a></li>
              <?php endfor;?>
              <li class="page-item" class="<?php if ($page >= $total_page) {
                                              echo 'disabled';
                                            } ?>">
                <a class="page-link" href="<?php if ($page >= $total_page) {
                                              echo '#';
                                            } else {
                                              echo "?page=" . ($page + 1);
                                            } ?>">suivante</a>
              </li>
            </ul>
          </nav>
    </div>
<?php
require_once("../config/footer.php");
?>
