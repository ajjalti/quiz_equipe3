<?php
require_once "../view/config/header.php";
?>
<div class="w-75 m-auto mt-5">
    <h6>liste des questions qui appartients au examen</h6>
<table border="1" class="table table-tesponsive table-stripped table-hover">
            <thead>
            <tr>
                <th>
                    <input type="checkbox" name="deleteAll" id="deleteAll" onclick="selectAllD('deleteChoix')">
                    select all
                </th>
                <th>#</th>
                <th>question</th>
                <th>action</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($questionsPourExamen as $question) : ?>
                        <tr data-id=<?= $question->getId() ?>>
                            <td><input type="checkbox" name="deleteChoix" id=""></td>
                            <td><?= $question->getId() ?></td>
                            <td>
                            <?= $question->getLib() ?>
                            </td>
                            <td><button class="btn btn-warning" onclick="delet(<?= $question->getId() ?>)">modifier</button></td>
                        </tr>
                <?php endforeach;
                ?>
            </tbody>
        </table>
        <button class="btn btn-danger mb-5" onclick="deleteAll()">supprimer</button>

        <h6>ajouter d'autre question au examen</h6>
            <table border="1" class="table table-tesponsive table-stripped table-hover">
            <thead>
            <tr>
                <th>
                <input type="checkbox" name="addAll" id="addAll" onclick="selectAllA('addChoix')">
                    select all
                </th>
                <th>#</th>
                <th>question</th>
                <th>action</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($examenPourCompetence as $question) : ?>
                    <!-- <form method="post"> -->
                        <tr data-id=<?= $question->getId() ?>>
                            <td><input type="checkbox" name="addChoix" id="" ></td>
                            <input hidden type="text" value="<?= $question->getId() ?>" name="id">
                            <td><?= $question->getId() ?></td>
                            <td>
                            <?= $question->getLib() ?>
                            </td>
                            <td><button class="btn btn-warning" onclick="add(<?= $question->getId() ?>)">modifier</button></td>
                        </tr>
                    <!-- </form> -->
                <?php endforeach;
                ?>
            </tbody>
                </table>
                <button class="btn btn-info d-block mb-3" onclick="addAll()">ajouter</button>
            <a href="../view/examen/examens" class="btn btn-success mb-5">terminer édition</a>
    </div>

<script>
    const listD = document.getElementsByName("deleteChoix")
    // add event listener:
    listD.forEach(element => {
        element.addEventListener("click", function(e){
        if(e.target.checked){
            e.target.parentElement.parentElement.classList.add("bg-secondary")
        }else{
            e.target.parentElement.parentElement.classList.remove("bg-secondary")
        }
     })
    })
    const listA = document.getElementsByName("addChoix")
    // add event listener:
    listA.forEach(element => {
        element.addEventListener("click", function(e){
        if(e.target.checked){
            e.target.parentElement.parentElement.classList.add("bg-secondary")
        }else{
            e.target.parentElement.parentElement.classList.remove("bg-secondary")
        }
     })
    })
    const selectAllD = (name)=>{
        const check = document.querySelector("#deleteAll")
        if(check.checked){
            const list = document.getElementsByName(name)
            for(let i=0; i<list.length; i++){
                list[i].checked=true
                list[i].parentElement.parentElement.classList.add("bg-secondary")
            }
        }
        if(!check.checked){
            const list = document.getElementsByName(name)
            for(let i=0; i<list.length; i++){
                list[i].checked=false
                list[i].parentElement.parentElement.classList.remove("bg-secondary")
            }
        }
    }
    const selectAllA = (name)=>{
        const check = document.querySelector("#addAll")
        if(check.checked){
            const list = document.getElementsByName(name)
            for(let i=0; i<list.length; i++){
                list[i].checked=true
                list[i].parentElement.parentElement.classList.add("bg-secondary")
            }
        }
        if(!check.checked){
            const list = document.getElementsByName(name)
            for(let i=0; i<list.length; i++){
                list[i].checked=false
                list[i].parentElement.parentElement.classList.remove("bg-secondary")
            }
        }
    }
    const modifier = (id)=>{
        // ajout du code pour redirection vers la partie modifier question

    }
    const addAll = ()=>{
        const array_question=[]
        const questions_tr= document.querySelectorAll('.bg-secondary')
        questions_tr.forEach(element=>{
            array_question.push(element.getAttribute('data-id'))
        })
        const postData = new FormData()
        postData.append("id_questions",JSON.stringify(array_question))
        postData.append("id_examen",<?= $_GET['id'] ?>)
        console.log(postData);
        fetch("../api/examen/ajouterQuestion.php",{
            method:"POST",
            body:postData
        }).then(function(){
            window.location.reload();
        })
    }
    const deleteAll = ()=>{
        const array_question=[]
        const questions_tr= document.querySelectorAll('.bg-secondary')
        questions_tr.forEach(element=>{
            array_question.push(element.getAttribute('data-id'))
        })
        const postData = new FormData()
        postData.append("id_questions",JSON.stringify(array_question))
        postData.append("id_examen",<?= $_GET['id'] ?>)
        fetch('../api/examen/supprimerQuestion.php', {
        method: 'POST',
        body: postData
        }).then(function(){
            window.location.reload();
        })
    }
</script>

<?php
require_once("../view/config/footer.php");
?>