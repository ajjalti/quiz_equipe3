<?php
require_once("../modules/Question.php");
require_once("../modules/Competence.php");
require_once("../modules/Examen.php");
require_once("../Connection.php");
class ExamenController
{

    private Connection $conn;

    public function __construct()
    {
        $this->conn = new Connection();
    }
    public function retournerQuestionPourExamen($idExamen){
        $examen = new Examen();
        $examen->setId((int)$idExamen);
        return $examen->getQuestionInExam($this->conn->connect()) ;

    }
    public function retournerQuestionPourCompetence($idExamen){
        $competence = new Competence();
        $competence->setId((int)$_SESSION['competence']);
        $allQuestions=$competence->getQuestionForCompetence($this->conn->connect());
        $questions=[];
        if(isset($_SESSION['competence'])):
        foreach ($allQuestions as $question) {
        if(!in_array($question,$this->retournerQuestionPourExamen($idExamen))) array_push($questions,$question);
    }
        endif; 
        return $questions;
    }
}
