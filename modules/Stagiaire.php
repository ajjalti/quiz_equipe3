<?php

declare(strict_types=1);

class Stagiaire
{

    private int $id;

    private string $nom;
    private string $prenom;

    private string $email;

    private string $password;
    private string $idGroupe;

  
    public function __construct()
    {
    }



    /**
     * @return [object Object]
     */
    
    public function save()
    {
        // TODO implement here
        return null;
    }

    /**
     * @return [object Object]
     */
    public function update()
    {
        // TODO implement here
        return null;
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        // TODO implement here
        return false;
    }

    /**
     * @return array
     */
    public static function all(): array
    {
        // TODO implement here
        return [];
    }

    /**
     * @param int $CEF 
     * @return [object Object]
     */
    public static function findByCEF(int $CEF)
    {
        // TODO implement here

        return $CEF;
    }

    /**
     * @return [object Object]
     */
    public function group()
    {
        // TODO implement here
        return null;
    }

    /**
     * @return [object Object]
     */
    public function filiere()
    {
        // TODO implement here
        return null;
    }
    // login

    public static function login(PDO $conn, string $email, string $password)
    {
        try {
            $query = "SELECT * FROM `stagiaire` WHERE `email` = ? ";
            $pdoS = $conn->prepare($query);
            $pdoS->execute([
                $email
            ]);
            if ($pdoS->rowCount() > 0) {
                $staigaire_row = $pdoS->fetch();
                
                if ($staigaire_row->password == $password) {
                    return new self($staigaire_row->id, $staigaire_row->nom, $staigaire_row->prenom, $staigaire_row->email, $staigaire_row->password,$staigaire_row->idGroupe );
                }
            }

            return false;
        } catch (\Throwable $th) {
            return false;
        }
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nom
     */ 
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */ 
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of prenom
     */ 
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set the value of prenom
     *
     * @return  self
     */ 
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of password
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */ 
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of idGroupe
     */ 
    public function getIdGroupe()
    {
        return $this->idGroupe;
    }

    /**
     * Set the value of idGroupe
     *
     * @return  self
     */ 
    public function setIdGroupe($idGroupe)
    {
        $this->idGroupe = $idGroupe;

        return $this;
    }
}
