<?php

declare(strict_types=1);

use function PHPSTORM_META\type;

class Examen
{

    /** @var int */
    private int $id;

    /** @var int */
    private int $idCompetence;

    /** @var string */
    private string $lib;

    /** @var date */
    private  $dateCreation;

    /** @var date */
    private  $datePassation;
        /** @var array<Question> */
        private  $questions;
    /**
     * Default constructor
     */
    public function __construct()
    {
        // ...
    }

    //! partie getters and setters :
    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;
        
        return $this;
    }
    /**
    * Set the value of lib
    *
    * @return  self
    */ 
   public function setLib($lib)
   {
       $this->lib = $lib;

       return $this;
   }
   /**
    * Get the value of lib
    */ 
   public function getLib()
   {
       return $this->lib;
   }
    
    /**
     * Get the value of idCompetence
     */
    public function getIdCompetence()
    {
        return $this->idCompetence;
    }

    /**
     * Set the value of idCompetence
     *
     * @return  self
     */
    public function setIdCompetence($idCompetence)
    {
        $this->idCompetence = $idCompetence;

        return $this;
    }
    /**
     * Get the value of datePassation
     */
    public function getDatePassation()
    {
        return $this->datePassation;
    }

    /**
     * Set the value of datePassation
     *
     * @return  self
     */
    public function setDatePassation($datePassation)
    {
        $this->datePassation = $datePassation;

        return $this;
    }

    /**
     * Get the value of dateCreation
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set the value of dateCreation
     *
     * @return  self
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }
            /**
         * Get the value of questions
         */ 
        public function getQuestions()
        {
                return $this->questions;
        }
        public function getLibExamen()
        {
            return $this->lib;
        }

        /**
         * Set the value of questions
         *
         * @return  self
         */ 
        public function setQuestions(array $questions)
        {
                $this->questions = $questions;

                return $this;
        }

    /**
     * @return Examen
     */
    public function save(PDO $conn)
    {
        try {
            $query = "UPDATE `examen` 
                        SET `lib`=?,
                        `datePassation`=? 
                        WHERE `id` = ?";
            $pdoS = $conn->prepare($query);
            $pdoS->execute([
                $this->lib,
                $this->datePassation,
                $this->id,
            ]);
            return $this;
        } catch (\Throwable $th) {
            return false;
        }
    }

    /**
     * @return self
     */
    public function update(PDO $conn , string $label, $datePassation) //  Modifier Examen
    {
        try {
            $this->setLib($label);
            $this->setDatePassation($datePassation);
            return $this->save($conn);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    /**
    *@return bool
     */    
    public function delete(PDO $conn,$id)
    {
        // delete from pour
        $query1 = "delete from pour where idExamen=?";
        //get ids evaluations:
        // return array of evaliations id:
        $query2="select id from evaluation where idExamen=?";
        // delete from etre choisi
        $query3 ="delete from etre_choisi where idEvaluation=?";
        // delete from evaluation
        $query4="delete from evaluation where idExamen=?";
        // delete from examen
        $query5="delete from examen where id=?";
        $conn->beginTransaction();
        try {
            // ferst step:
            $pdos1 = $conn->prepare($query1);
            $pdos1->execute([$id]);
            // second step:
            $pdos2 = $conn->prepare($query2);
            $pdos2->execute([$id]);
            $evaluationsId = $pdos2->fetchAll();
            foreach ($evaluationsId as $idEvaluation){
                $pdos3 = $conn->prepare($query3);
                $pdos3->execute([$idEvaluation->id]);
            }
            // therd step :
            $pdos4 = $conn->prepare($query4);
            $pdos4->execute([$id]);
            // final step:
            $pdos5=$conn->prepare($query5);
            $pdos5->execute([$id]);
            $conn->commit();
        } catch (\Throwable $th) {
            $conn->rollBack();
            throw $th;
        }
    }

    /** editer les questions dans un exmen
     * @return bool
     */
    public function editer(PDO $conn,bool $ajouter=false,bool $supprimer=false)
    {
        if($ajouter==true){
            // ajouter des question pour l'examen selectionner
            $query="";
        }
        try {
            $query = "DELETE FROM `examen`  WHERE `id` = ?";
            $pdoS = $conn->prepare($query);
            $pdoS->execute([$this->id]);
        } catch (\Throwable $th) {
            //throw $th;
        }
        // TODO implement here
        return false;
    }

    /**
     * @return array
     */
    public static function all(): array
    {
        // TODO implement here
        return [];
    }

    /**
     * @return Examen|bool
     */
    public static function findByCode()
    {
        // TODO implement here
        return null;
    }


  

    public static function returnExamen(PDO $conn, $id)
    {
        try {
            $query = "SELECT * FROM `examen` 
            WHERE  `id` = ?";
            $pdoS = $conn->prepare($query);

            $pdoS->execute([
                $id,  
            ]);
            return $pdoS->fetchAll(PDO::FETCH_CLASS, 'Examen');
        } catch (\Throwable $th) {
            print_r($th);
            return false;
        }
    }

    public static function modifierExamen(PDO $conn, $id,$lib,$datePassation){
        {
            try {
                $query = "UPDATE `examen` 
                            SET `lib`=?,
                            `datePassation`=? 
                            WHERE `id` = ?";
                $pdoS = $conn->prepare($query);
                $pdoS->execute([
                    $lib,
                    $datePassation,
                    $id,
                ]);
            } catch (\Throwable $th) {
                return false;
            }
        }
    }
    public  function ajouterExamen(PDO $conn,$idCompetence,$idFormateur,$libExamen,$dateCreation,$datePassation){
        {
            try {
                $query = "INSERT INTO `examen`(`idCompetence`,`idFormateur`, 
                `lib`, `dateCreation`, `datePassation`)
                 VALUES (?,?,?,?,?)";
                $pdoS = $conn->prepare($query);
                $pdoS->execute([
                    $idCompetence,
                    $idFormateur,
                    $libExamen,
                    $dateCreation,
                    $datePassation
                ]);
                
                return $conn->lastInsertId();
            } catch (\Throwable $th) {
            }
        }
    }
    public function addQuestionsToExamen(){
        

    }
    //! methods ajouter 
    //!debut
        /**
     * get the value of the competence
     *
     * @return  self
     */
    public static function getCompetence(PDO $connexion,$id_competence)
    {

        $query = "select lib from competence where id=?";
        try{
            $statement = $connexion->prepare($query);
            $statement->execute([
                $id_competence
            ]);
            return $statement->fetchColumn();
        }catch(PDOException $e){
            echo $e->getMessage();
            die("Failed to query lib");
        }

    }

        /**
     * get the value of the filiere
     *
     * @return  self
     */
    public static function getFiliere(PDO $connexion,$id_filiere)
    {
        $query = "SELECT lib from filiere where id=?";
        try{
            $statement = $connexion->prepare($query);
            $statement->execute([
                $id_filiere
            ]);
            return $statement->fetchColumn();
        }catch(PDOException $e){
            echo $e->getMessage();
            die("Failed to query lib");
        }

    }
        /**
     * Set the value of module
     *
     * @return  self
     */
    public static function getModule(PDO $connexion,$id_module)
    {
        $query = "select lib from module where id=?";
        try{
            $statement = $connexion->prepare($query);
            $statement->execute([
                $id_module
            ]);
            return $statement->fetchColumn();
        }catch(PDOException $e){
            echo $e->getMessage();
            die("Failed to query lib");
        }

    }
    /**
     * Get the value of table count
     * @return number
    */
    public static function getLastId(PDO $connexion){
        $query="select id from examen ORDER by id DESC LIMIT 1";
        try{
            $statement=$connexion->query($query);
            return $statement->fetchColumn();
        }catch(PDOException $e){
            echo $e->getMessage();
            die("row count problems");
        }
    }
    //!fin
        /**
     * get questions in examen
     *
     * @return  questions [] or false
     */
    public function getQuestionInExam(PDO $connexion){
        $quesry = "SELECT * from pour where idExamen=?";
        try{
            $statement=$connexion->prepare($quesry);
            $statement->execute([
                $this->id
            ]);
            $data = $statement->fetchAll();
            $results=[];
            foreach ($data as $donne) {
                $query="select * from question where id=?";
                $statement=$connexion->prepare($query);
                $statement->execute([$donne->idQuestion]);
                $qu=$statement->fetchObject('Question');
                array_push($results,$qu);
            }
            return $results;
        }catch(PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }
     /**
     * delete questions from examen
     *
     * @return  void
     */
    public function deleteQuestionFromExam(PDO $connexion,array $questions_id){
        $query ="delete from pour where idExamen=? and idQuestion=?";
        try{
            foreach($questions_id as $question):
            $statement=$connexion->prepare($query);
            $statement->execute([
                $this->id,
                $question
            ]);
            endforeach;
            header("../editer.php?id=$this->id");
        }catch(PDOException $e){
            echo $e->getMessage();
            die("deleteQuestionFromExam faild!");
        }
    }
         /**
     * add questions in examen
     *
     * @return  void
     */

    public function addQuestionInExam(PDO $connexion,array $questions_id){
        $query ="INSERT into  pour (idExamen,idQuestion) values(?,?)";
        try{
            foreach($questions_id as $question):
                echo $question;
            $statement=$connexion->prepare($query);
            $statement->execute([
                $this->id,
                (int)$question
            ]);
            endforeach;
            header("../editer.php?id=$this->id");
        }catch(PDOException $e){
            echo $e->getMessage();
            die("addQuestionFromExam faild!");
        }
    }
    //? pour la pagination:
    public static function pagination(PDO $connexion,$numeroDePage,$idCompetence){
        $nombreDeLigneAAfficher = 10;
        $limitInitial = ($numeroDePage - 1) * $nombreDeLigneAAfficher;
        // nombre de ligne 
        $query = "SELECT * FROM examen where idCompetence=$idCompetence";
        $totalLignes = $connexion->query($query)->rowCount();
        $total_pages = ceil($totalLignes / $nombreDeLigneAAfficher);

        // retourner les ligne a afficher + le nombre total de page
        $query = "SELECT * FROM examen where idCompetence=?  LIMIT $limitInitial, $nombreDeLigneAAfficher";
        $pdos = $connexion->prepare($query);
        $pdos->execute([$idCompetence]);
        $data = $pdos->fetchAll(PDO::FETCH_CLASS, 'Examen');
        return ['data' => $data, 'totalPages' => $total_pages];
    }



}
