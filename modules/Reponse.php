<?php

declare(strict_types=1);


class Reponse
{

    private  $id;

    private string $lib;

    /**
     * Default constructor
     */
    public function __construct()
    {
        // ...
    }

    /**
     * @return [object Object]
     */
    public function save()
    {
        // TODO implement here
        return null;
    }

    /**
     * @return [object Object]
     */
    public function update()
    {
        // TODO implement here
        return null;
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        // TODO implement here
        return false;
    }

    /**
     * @return array
     */
    public static function all(): array
    {
        // TODO implement here
        return [];
    }

    /**
     * @param  $id 
     * @return Reponse|bool
     */

    public static function findById(PDO $conn, $id)
    {
        $query="select * from reponse where id=?";
        try{
            $statement=$conn->prepare($query);
            $statement->execute([$id]);
            $data= $statement->fetchAll(PDO::FETCH_CLASS,'Reponse')[0];
            return $data;
        }catch(PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }


    /**
     * Get the value of lib
     */ 
    public function getLib()
    {
        return $this->lib;
    }

    /**
     * Set the value of lib
     *
     * @return  self
     */ 
    public function setLib($lib)
    {
        $this->lib = $lib;

        return $this;
    }
}
