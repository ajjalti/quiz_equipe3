<?php

declare(strict_types=1);


class Question
{

    /** @var  */
    private  $id;

    /** @var string */
    private string $lib;

    /** @var int */
    private  $idReponse;

    /** @var int */
    private  $idCompetence;

    /**
     * Default constructor
     */
    public function __construct()
    {

    }

    /**
     * 
     */
    public function Operation1()
    {
        // TODO implement here
    }

    /**
     * 
     */
    public function Operation2()
    {
        // TODO implement here
    }

    /**
     * @return [object Object]
     */
    public function save()
    {
        // TODO implement here
        return null;
    }

    /**
     * @return [object Object]
     */
    public function update()
    {
        // TODO implement here
        return null;
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        // TODO implement here
        return false;
    }

    /**
     * @return array
     */
    public static function all(): array
    {
        // TODO implement here
        return [];
    }

    /**
     * @param  $id 
     * @return Question|bool
     */
    public static function findById(PDO $conn, $id)
    {
        $query="select * from question where id=?";
        try{
            $statement=$conn->prepare($query);
            $statement->execute([$id]);
            $data = $statement->fetchAll(PDO::FETCH_CLASS,'Question')[0];
            return $data;
        }catch(PDOException $e){
            echo $e->getMessage();
            return false;
        }
        
    }

    /**
     * @return Collection <Reponse>
     */

    public function reponsePossible()
    {
        // TODO implement here
        return null;
    }

    /**
     * @return [object Object]
     */

    public function reponseCorrect()
    {
        // TODO implement here
        return null;
    }


    /**
     * Get the value of libQuestion
     */ 
    public function getLibQuestion()
    {
        return $this->lib;
    }

    /**
     * Get the value of idReponse
     */ 
    public function getIdReponse()
    {
        return $this->idReponse;
    }

    /**
     * Get the value of idCompetence
     */ 
    public function getIdCompetence()
    {
        return $this->idCompetence;
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of lib
     */ 
    public function getLib()
    {
        return $this->lib;
    }

    /**
     * Set the value of lib
     *
     * @return  self
     */ 
    public function setLib($lib)
    {
        $this->lib = $lib;

        return $this;
    }
}
