<?php

declare(strict_types=1);

//  FIXME : This Page Should Be Deleted //

class DetailEvaluation
{

    /** @var int */
    private int $idEvaluation;

    /** @var int */
    private int $idReponse;

    /** @var int */
    private int $idQuestion;

    /**
     * Default constructor
     */

    public function __construct()
    {
        // ...
    }

    /**
     * @return [object Object]
     */
    public function save()
    {
        // TODO implement here
        return null;
    }

    /**
     * @return [object Object]
     */
    public function update()
    {
        // TODO implement here
        return null;
    }

    /**
     * 
     * @return DetailEvaluation
     */
    public static function findByEvaluation(PDO $conn, int $idEvaluation)
    {
        try {
            $query = "SELECT * FROM `etre_choisi` WHERE `idEvaluation` = ?";
            $pdoS = $conn->prepare($query);
            $pdoS->execute([
                $idEvaluation
            ]);
            $fetch_resultas =  $pdoS->fetchAll(PDO::FETCH_CLASS,'DetailEvaluation');
        } catch (\Throwable $th) {
            throw $th;
        }
        return $fetch_resultas;
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        // TODO implement here
        return false;
    }

    /**
     * @return array
     */
    public static function all(): array
    {
        // TODO implement here
        return [];
    }

    /**
     * @return Staigaire
     */
    public function staigaire()
    {
        // TODO implement here
        return null;
    }

    /**
     * @return [object Object]
     */
    public function evaluation()
    {
        // TODO implement here
        return null;
    }

    /**
     * @return [object Object]
     */
    public function reponse()
    {
        // TODO implement here
        return null;
    }

    /**
     * @return Quesion
     */
    public function question()
    {
        // TODO implement here
        return null;
    }

    /**
     * @param int $id_stagiaire
     * @param int $id_evaluation 
     * @param int $id_question 
     * @return DetailEvaluation|boll
     */
    public function find(int $id_stagiaire, int $id_evaluation, int $id_question)
    {
        // TODO implement here
        return null;
    }


    /**
     * Get the value of idEvaluation
     */ 
    public function getIdEvaluation()
    {
        return $this->idEvaluation;
    }

    /**
     * Set the value of idEvaluation
     *
     * @return  self
     */ 
    public function setIdEvaluation($idEvaluation)
    {
        $this->idEvaluation = $idEvaluation;

        return $this;
    }

    /**
     * Get the value of idReponse
     */ 
    public function getIdReponse()
    {
        return $this->idReponse;
    }

    /**
     * Set the value of idReponse
     *
     * @return  self
     */ 
    public function setIdReponse($idReponse)
    {
        $this->idReponse = $idReponse;

        return $this;
    }

    /**
     * Get the value of idQuestion
     */ 
    public function getIdQuestion()
    {
        return $this->idQuestion;
    }

    /**
     * Set the value of idQuestion
     *
     * @return  self
     */ 
    public function setIdQuestion($idQuestion)
    {
        $this->idQuestion = $idQuestion;

        return $this;
    }
}
