<?php

declare(strict_types=1);


class Competence
{

    /** @var int */
    private int $id;

    /** @var String */
    private String $lib;

    /** @var int */
    private int $idModule;

    /**
     * Default constructor
     */
    public function __construct()
    {
        // ...
    }

    /**
     * @return [object Object]
     */
    public function save()
    {
        // TODO implement here
        return null;
    }

    /**
     * @return [object Object]
     */
    public function update()
    {
        // TODO implement here
        return null;
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        // TODO implement here
        return false;
    }

    /**
     * @return array
     */
    public static function all(): array
    {
        // TODO implement here
        return [];
    }

    /**
     * @param int $id 
     * @return Competence|bool
     */
    public static function findById(int $id)
    {
        // TODO implement here
        return null;
    }

    /**
     * @return [object Object]
     */
    public function module()
    {
        // TODO implement here
        return null;
    }
    public static function returnerExamens(PDO $conn, int $idCompetence)
    {
        try {
            $query = "SELECT * FROM `examen` 
            WHERE  `idCompetence` = ?";
            $pdoS = $conn->prepare($query);

            $pdoS->execute([
                $idCompetence,  
            ]);


            return $pdoS->fetchAll(PDO::FETCH_CLASS, 'Examen');
        } catch (\Throwable $th) {
            print_r($th);
            return false;
        }
    }
    public  function returner_Examens(PDO $conn)
    {
        try {
            $query = "SELECT * FROM `EXAMEN` 
            WHERE  `idCompetence` = ?";
            $pdoS = $conn->prepare($query);

            $pdoS->execute([
                $this->id,
            ]);


            return $pdoS->fetchAll(PDO::FETCH_CLASS, 'Module');
        } catch (\Throwable $th) {
            print_r($th);
            return false;
        }
    }

    /**
     * Get the value of lib
     */ 
    public function getLibCompetence()
    {
        return $this->lib;
    }

    /**
     * Set the value of lib
     *
     * @return  self
     */ 
    public function setLibCompetence($lib)
    {
        $this->lib = $lib;

        return $this;
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * retourner liste des questions
     *
     * @return  array<Question>
     */

    public function getQuestionForCompetence(PDO $connexion){
        $query = 'SELECT * from question where idCompetence=?';
        try{
            $statement = $connexion->prepare($query);
            $statement->execute([
                $this->id
            ]);
            $data =  $statement->fetchAll(PDO::FETCH_CLASS,'Question');
            return $data;
        }catch(PDOException $e){
            echo $e->getMessage();
            return false;
        }
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
