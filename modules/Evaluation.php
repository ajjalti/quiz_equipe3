<?php

declare(strict_types=1);


class Evaluation
{

    /** @var int */
    private int $id;

    /** @var int */
    private int $idExamen;

    /** @var int */
    private int $idStagiaire;

    /** @var string (Date) */
    private string $date;

    /** @var int */
    private int $score;

    /** @var array */
    private array $Questions;

    /** @var array */
    private array $Reponses;

    /**
     * Default constructor
     */
    public function __construct()
    {
        // ...
    }

    /**
     * @return [object Object]
     */
    public function save()
    {
        // TODO implement here
        return null;
    }

    /**
     * @return [object Object]
     */
    public function update()
    {
        // TODO implement here
        return null;
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        // TODO implement here
        return false;
    }

    /**
     * @return array
     */
    public static function all(): array
    {
        // TODO implement here
        return [];
    }

     /**
     * @return array
     */
    public static function findAllByFormateur(PDO $conn, $formateurId): array
    {
        try {
            $query = "SELECT * FROM `evaluation` WHERE `idExamen` in (SELECT id FROM `examen` WHERE `idFormateur` = ?) ";
            $pdoS = $conn->prepare($query);
            $pdoS->execute([
                $formateurId
            ]);
            $fetch_resultas =  $pdoS->fetchAll(PDO::FETCH_CLASS,'Evaluation');
            return $fetch_resultas;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * @param  $id 
     * @return [object Object]
     */    
    /**
     * findById
     *
     * @param  mixed $id
     *
     */
    public static function findById(PDO $connexion,$id)
    {
        $query="select * from evaluation where id=?";
        try{
            $statement=$connexion->prepare($query);
            $statement->execute([$id]);
            return $statement->fetchAll(PDO::FETCH_CLASS,'Evaluation');
        }catch(PDOException $e){
            echo $e->getMessage();
            return null;
        }
    }

    /**
     * @return Examen Object
     */
    public function getExamen(PDO $connexion)
    {
        $query="select * from examen where id=?";
        try {
            $statement = $connexion->prepare($query);
            $statement->execute([
                $this->idExamen
            ]);
           $obj = $statement->fetchAll(PDO::FETCH_CLASS,'Examen');
            return $obj[0];
        } catch (\Throwable $th) {
            throw $th;
            return null;
        }
    }

    /**
     * @return Staigaire
     */
    public function getStagiaire(PDO $connexion,$idStagiaire)
    {
        $query="select * from stagiaire where id=?";
        try {
            $statement = $connexion->prepare($query);
            $statement->execute([
                $idStagiaire
            ]);
            return $statement->fetchAll(PDO::FETCH_CLASS,'Stagiaire')[0];
        } catch (\Throwable $th) {
            throw $th;
            return false;
        }
        return null;
    }


    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of date
     */ 
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */ 
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the value of score
     */ 
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set the value of score
     *
     * @return  self
     */ 
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get the value of idExamen
     */ 
    public function getIdExamen()
    {
        return $this->idExamen;
    }

    /**
     * Set the value of idExamen
     *
     * @return  self
     */ 
    public function setIdExamen($idExamen)
    {
        $this->idExamen = $idExamen;

        return $this;
    }

    /**
     * Get the value of idStagiaire
     */ 
    public function getIdStagiaire()
    {
        return $this->idStagiaire;
    }

    /**
     * Set the value of idStagiaire
     *
     * @return  self
     */ 
    public function setIdStagiaire($idStagiaire)
    {
        $this->idStagiaire = $idStagiaire;

        return $this;
    }
}
