<?php
session_start();
if(isset($_GET['id'])):
require_once "../controllers/ExamenController.php";
$idExamen = $_GET['id'];
// retourner les questions d'un examen:
$examenController = new ExamenController();
$questionsPourExamen = $examenController->retournerQuestionPourExamen($idExamen);

// retourner tous les questions apart celle déjà qui appartient au examen
$examenPourCompetence = $examenController->retournerQuestionPourCompetence($idExamen);
require_once "../view/examen/editer.php";
else:
    header("location:./loginRouter");
endif;